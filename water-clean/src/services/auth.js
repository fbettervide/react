import { getSessionValue } from './SessionManager';

export const isAuthenticated = () => {

    // Lógica para buscar se existe token em localStorage
    // pode ser usado getSessionValue de SessionManager
    return  getSessionValue('token') ? true : false;;
    // return false;

};
