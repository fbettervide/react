

export const resetSession = () => sessionStorage.clear();

export const setSessionValue = (pParam, pValue) => sessionStorage.setItem(pParam, pValue);

export const getSessionValue = (pParam) => sessionStorage.getItem(pParam) || '';

export const removeSessionValue = (pValue) => sessionStorage.removeItem(pValue);

export const clearSession = () => sessionStorage.clear();

