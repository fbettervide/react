import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Modal from 'react-modal';
import { connect } from 'react-redux';
import { getData, setModalState } from './actions/poolActions';

Modal.setAppElement('#root');

class ModalWrap extends Component {
    constructor(props) {
        super(props);
        this.state = { modalIsOpen: false };

        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    openModal = (data=null) => {
        this.setState({ modalIsOpen: true });
    }

    afterOpenModal() {
        // this.subtitle.style.color = '#f00';
    }

    closeModal() {
        // this.setState({ modalIsOpen: false });
        this.props.dispatch(setModalState(false));
    }

    renderComponent = () => {
        let CompName = this.props.modalContent;
        return (
            <CompName />
        )
    }

    render() {
        return (
            <Modal
                    isOpen={ this.props.data.pool.modalState }
                    onAfterOpen={ this.afterOpenModal }
                    onRequestClose={ this.closeModal }
                    contentLabel="Example Modal"
                >
                <div id="modalWrap">
                    <div className="modalTitleWrap">
                        <span className="modalClose pull-right" onClick={ this.closeModal }>
                            x
                        </span>
                    </div>
                    <div className="modalContent">
                        { this.props.children }
                    </div>
                </div>
            </Modal>
        )
    }
}

const mapStateToProps = (state) => ({ data: state });

export default connect( mapStateToProps)(ModalWrap);