import React from 'react';

function clearSession() {
  sessionStorage.clear();
}

function Layout() {

  return (
    <nav className="navbar navbar-inverse">
      <div className="container-fluid">
        <div className="navbar-header">
          <a className="navbar-brand" href="#">WaterClean</a>
        </div>
        <ul className="nav navbar-nav">
          <li className="active"><a href="/agenda">Agenda</a></li>
          <li><a href="/piscinas">Piscinas</a></li>
        </ul>
        <div className="navbar-header pull-right">
        <a className="navbar-brand" href="/"  >Sair</a>
        </div>
      </div>
    </nav>
  );
}

export default Layout;