import produce from 'immer';
import { 
    MODAL_STATE
} from '../actions/poolActions';

const initialData = {
    pool: { name: '', description: '' },
    address: [ { address_type: '', city: '', complement: '', id: '', logradouro: '', neighborhood: '', number: '', state: '', status: '', zip: '' } ],
    contacts: [ { contact_type: '', contact_id: '', contact_person: '', master: '', name: '', status: '', value: '' } ]
}

const initialState = {
    data: {
        ...initialData
    },
    original: {
        ...initialData
    }
}

export default function poolReducer(state = initialState, action) {
    switch (action.type) {
        case MODAL_STATE:
            return produce(state, draft => {
                draft.modalState = action.state
            });
        default:
            return state;
    }
}