import { combineReducers } from "redux";
import pool from "./poolReducer";

export default combineReducers({
    pool
})