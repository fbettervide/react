import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import ScheduleList from './pages/index.js';
import Pool from './pages/pool.js';
import Login from './login.js';
import Cleaning from './pages/cleaning.js';
import Profile from './pages/profile.js';
import DetailCleaning from './pages/detailCleaning.js';
import AddPool from './pages/addPool.js';
import RegisterCleaning from './pages/registerCleaning.js';
import ScheduleAdd from './pages/scheduleAdd.js';
import Layout from './Layout.js';
import {isAuthenticated} from './services/auth';


const PrivateRoute = ({ component: Component, ... rest }) => (
    
    <Route { ...rest } render={ props => (
        isAuthenticated() ? (
            <React.Fragment>
                <div className='App' id='App'>
                    <Layout />
                    <div id="mainContent">
                        <Component { ...props } />
                    </div>
                </div>
            </React.Fragment>
        ) : (
            <Redirect to={{ pathname: "/", state: { from: props.location } }} />
        ) 
    )} />
);


const Routes = () => (
    <BrowserRouter> 
        <Switch>
            <Route exact path="/" component={Login} />
            <PrivateRoute path="/agenda" component={ScheduleList} />
            <PrivateRoute path="/agendar/:id" component={ScheduleAdd} />
            <PrivateRoute path="/piscinas" component={Pool} />
            <PrivateRoute path="/piscina/adicionar" component={AddPool} />
            <PrivateRoute path="/limpezas/:id" component={Cleaning} />
            <PrivateRoute path="/registrar/:id" component={RegisterCleaning} />
            <PrivateRoute path="/detalhes/:id" component={DetailCleaning} />
            <PrivateRoute path="/perfil" component={Profile} />
        </Switch>
    </BrowserRouter>
);

export default Routes;