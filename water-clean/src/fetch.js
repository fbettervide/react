// export const getData = (url) => {
//     let data = fetch(url)
//         .then(res => res.json())
//         .then(data => {return data})
//     // console.log(data);
//     return data
// }

export const fetchData = (url) => dispatch => {
    dispatch(requestData());
    return getData(url).then(data => {
        // simulated delay
        setTimeout(() => {
            if (data.message) {
                alert(data.message);
                dispatch(requestError());
            }
            else return dispatch(getList(data));
        }, 1000);
    })
    .catch(error => {
        console.dir(error);
        alert('Error');
        dispatch(requestError());
    })
}

const getData = (url) => {
    return fetch(url).then(response => (
        response.ok ? response.json() : { message: "Erro de conexão" }
    ));
}