import React, { Component } from 'react';
import { setSessionValue, resetSession } from './services/SessionManager';
import styles from './css/login.css';
import axios from 'axios';
import ScheduleList from './pages';
import { url } from './path';

class Login extends Component { 

  url  = url(); 

  constructor(props) {
    super(props);

    this.state = {
      login : '',
      password: '',
      auth: false
    };

    this.onChange = this.onChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  onChange(event) {
    console.log(event.target.name);
    switch (event.target.name) {
      case 'login':
        this.setState({ login: event.target.value });
        break;
      case 'password':
        this.setState({ password: event.target.value });
        break;
    }
  }

  handleSubmit(event) {
    event.preventDefault();

    const config = {
      headers: {
        'Content-Type': "application/json"
      }
    }

    axios
        .post(this.url + "/auth", {
          "userName": this.state.login,
          "password": this.state.password
        }, config)
        .then(function(response) {
          debugger;
          setSessionValue('token', response.data);
          this.state.auth = true;
          window.location.href = "/agenda";

        }.bind(this)).catch(function (error) {
            console.log(error);
        });
  }


  render() {
      if(!this.state.auth) {
        resetSession();
      }
      return (
        <div class="container">
          <div class="row">
            <div class="card card-container">
                <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
                <p id="profile-name" class="profile-name-card"></p>
                <form class="form-signin" onSubmit={this.handleSubmit} >
                    <span id="reauth-email" class="reauth-email"></span>
                    <input type="text" id="login" name="login" class="form-control" onChange={this.onChange} placeholder="Email address" required />
                    <input type="password" id="inputPassword" name="password" class="form-control" onChange={this.onChange} placeholder="Password" required />
                    <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit" >Entrar</button>
                </form>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <h6>Login e Senha para teste</h6>
              <p><b>Login:</b> felipe.pereira </p>
              <p><b>Senha:</b> testeabc </p>
            </div>
          </div>
        </div>
      );
    }
  
}

export default Login;