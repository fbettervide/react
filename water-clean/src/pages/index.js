import React, { Component } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Layout from '../Layout';
import axios from 'axios';
import { url } from '../path';
import { schedule } from '../css/schedule.css';
import Modal from 'react-modal';
import ModalWrap from '../Modal';
import { getData, setModalState, addPool  } from '../actions/poolActions';
import { connect } from 'react-redux';
import RegisterCleaning from './registerCleaning';

Modal.setAppElement('#root');
class ScheduleList extends Component {

  url  = url();

  state = {
    schedules: [],
    idPool: 0,
  };

  componentDidMount() {
    axios.get(this.url + '/agenda').then(res => {
      console.log(res);
      this.setState({ schedules: res.data });
    })
  }


  openModal = (data=null) => {
    this.state.idPool = data.currentTarget.value;
    this.props.dispatch(setModalState(true));
  }

  verificaAgenda(pool) {

    console.log("VERIFICA " + JSON.stringify(pool))

    if(pool) {
      return true;  
    }
    return false;
  }

  formataHora(str) {
    return str.substr(0,5);
  }

  formataData(date) {
    return new Date(date).toLocaleDateString();
  }


  render() {

    console.log('AGENDA ' + JSON.stringify(this.state.schedules));
    debugger;
    return (
      <div>
        <ModalWrap modalTitle={ 'Registro de Limpeza' }>
            <RegisterCleaning idPool={this.state.idPool} />
        </ModalWrap>
        <div class="container">
          <div class="row">
          {this.state.schedules[0] ?
            this.state.schedules.map(schedule => (
            <div class="col-sm-6 col-md-6">
              { this.verificaAgenda(schedule) ?
              <div class="thumbnail card-min">
                { schedule.image ? 
                  <img src={this.url + "/assets/uploads/" + schedule.image.toString()} alt='Imagem' />
                  : 
                  <img src={this.url + "/assets/uploads/teste.jpg"} alt='Imagem' /> }
                
                  <div class="col-xs-12">
                    <p class="tamFont"><span><b> {schedule.name_client}</b> </span></p>
                  </div>
                  <div class="caption">
                    <p><span><b>Data:</b> {this.formataData(schedule.date_check)}</span></p>
                    <p><span><b>Hora:</b> {this.formataHora(schedule.time_check)}</span></p>
                    <p><b>Status:</b> {schedule.status == 0 ? <span className="label label-warning">Aguardando Limpeza</span> : schedule.status == 1 ? <span className="label label-success">Limpeza Realizada</span> : ''}</p>
                  </div>
                  <hr />
                  <center>
                  {schedule.status == 0 ?
                    <button class="btn btn-primary" role="button" onClick={this.openModal} value={schedule.id} >Registrar Limpeza</button> 
                  : 
                    <a href={"/detalhes/" + schedule.reportId} class="btn btn-warning" role="button">Detalhes da Limpeza</a> 
                  }   
                  </center>
              </div>
              : ''}
            </div>
            )) : 
          <div class="alert alert-info">
            <strong>Seja bem vindo!</strong>  Nenhuma limpeza de piscina foi agendada.
          </div>}
          </div>
        </div>
      </div>
    );
  }

}


const mapStateToProps = (state) => ({ data: state });

export default connect(mapStateToProps)(ScheduleList);