import React, { Component } from 'react';
import { url } from '../path';
import AddPool from './addPool';
import Modal from 'react-modal';
import ModalWrap from '../Modal';
import { getData, setModalState, addPool  } from '../actions/poolActions';
import Schedule from '../css/schedule.css';
import ScheduleAdd from './scheduleAdd';
import { connect } from 'react-redux';

Modal.setAppElement('#root');
   
class ListPool extends Component {

  url  = url();

  constructor(props) {
    super(props);

    this.state = {
      value: 0,
      setValue: 0,
      register : [],
      states : [], 
      returnRegister: false,
      open: false,
      update: false,
      pools: [],
      add: false,
      agenda: false,
      poolSelected: 0
    };
  }

  

  

  render() {
    console.log("PROPS" + JSON.stringify())
    var button = {
      marginTop: '10px'
    };

    return (
        <div>
          <div className="container">
            <div className="row">
              <div className="card  col-md-12 col-xs-12">
                {this.props.children.map(pool => (
                    <div className="card card-intro col-md-6 col-xs-12">
                      <div className="col-md-4">
                      { pool.image ? 
                        <img className="card-img-bottom" src={this.url + '/assets/uploads/'  + pool.image} alt="Imagem de capa do card" width="100%" />
                      : <img className="card-img-bottom" src="https://img.olx.com.br/images/72/728806117572506.jpg" alt="Imagem de capa do card" width="100%" /> }
                      </div>
                      <div className="col-md-6">
                        <h5 className="card-title">{pool.name_client}</h5>
                        <p className="card-text">{pool.description}</p>
                        <p className="card-text"><small className="text-muted">Cliente deste : {pool.created_at}</small></p>
                      </div>
                      <div className="col-md-12">
                          <div className="col-md-4">
                            <a className="btn btn-primary" href={"/limpezas/" + pool.id} style={button}> Limpezas Efetuadas</a>
                          </div>
                          <div className="col-md-4">
                            <button id="btn-agenda" className="btn btn-default" style={button} onClick={this.props.openModal} value={pool.id}> Agendar Limpeza</button>
                          </div>
                          <div className="col-md-4">
                            <a className="btn btn-default" target="_blank" href={"https://api.whatsapp.com/send?text=http://watercleangierakonline.herokuapp.com/limpezas/" + pool.id} style={button}> Compartilhar </a>
                          </div>
                      </div>
                    </div>
                ))}
              </div>
            </div>
          </div>
        </div>
    );

  }

}

const mapStateToProps = (props) => ({ pool: props });

export default connect(mapStateToProps)(ListPool);
// export default ListPool;