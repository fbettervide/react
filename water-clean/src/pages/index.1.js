import React, { Component } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import ListSubheader from '@material-ui/core/ListSubheader';
import IconButton from '@material-ui/core/IconButton';
import AssignmentIcon from '@material-ui/icons/Assignment';
import Layout from '../Layout';
import axios from 'axios';
import Link from '@material-ui/core/Link';


class ScheduleList extends Component {

  state = {
    pools: [],
  };

  componentDidMount() {
    axios.get('http://127.0.0.1/agenda').then(res => {
      console.log(res);
      this.setState({ pools: res.data });
    })
  }

  classes() {
    return makeStyles(theme => ({
      root: {
        flexGrow: 1,
      },
      paper: {
        padding: theme.spacing(2),
        margin: 'auto',
        maxWidth: 500,
      },
      image: {
        width: 128,
        height: 128,
      },
      img: {
        margin: 'auto',
        display: 'block',
        maxWidth: '100%',
        maxHeight: '100%',
      },
    }));
  }

  render() {
    return (
      <div>
        <div>
          <Layout />
        </div>
        <div className={this.classes.root}>
          <GridList cellHeight={200} className={this.classes.gridList}>
            <GridListTile key="Subheader" cols={2} style={{ height: 'auto' }}>
              <ListSubheader component="div">Agenda</ListSubheader>
            </GridListTile>
            {this.state.pools.map(tile => (
              
                <GridListTile key={tile.img}>
                  <img src={tile.img} alt={tile.title} />
                  <Link href="/limpezas/1" >
                  <GridListTileBar
                    title={tile.title}
                    subtitle={<span>Cliente: {tile.author}</span>}
                    actionIcon={
                      <IconButton aria-label={`info about ${tile.title}`} className={this.classes.icon}>
                        <AssignmentIcon />
                      </IconButton>
                    }
                  />
                  </Link>
                </GridListTile>
              
            ))}
          </GridList>
        </div> 
      </div>
    );
  }

}


export default ScheduleList;