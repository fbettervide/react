import React, { Component } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Layout from '../Layout';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import axios from 'axios';
import styles from '../css/details-pool.css';
import { url } from '../path';
   
class DetailCleaning extends Component {

  url  = url();

  constructor(props) {
    super(props);

    this.state = {
      value: 0,
      setValue: 0,
      idCleaningPool: this.props.match.params.id,
      cleaning : [] 
    };

    this.handleChange = this.handleChange.bind(this);
    this.onChange = this.onChange.bind(this);
  }
  

  handleChange(event, newValue) {
    this.setState({ 
      setValue: newValue,
      value: newValue,
    });
  }

  onChange(event) {
    switch (event.target.name) {
      case 'description':
        this.state.cleaning.description = event.target.value;
        break;
      case 'checkIn':
        this.state.cleaning.checkIn = event.target.value;
        break;
      case 'checkOut':
        this.state.cleaning.checkOut = event.target.value;
        break;
      case 'levelChlorine':
        this.state.cleaning.levelChlorine = event.target.value;
        break; 
      case 'levelAlkalinity':
        this.state.cleaning.levelAlkalinity = event.target.value;
        break;
      case 'levelConditioner':
        this.state.cleaning.levelConditioner = event.target.value;
        break;
      case 'levelHardness':
        this.state.cleaning.levelHardness = event.target.value;
        break;
      case 'levelPhosphate':
        this.state.cleaning.levelPhosphate = event.target.value;
        break;
    }
    
  }

  componentDidMount() {

    console.log("ID " + this.state.idCleaningPool);

    axios.get(this.url + '/limpeza/' + this.state.idCleaningPool).then(res => {
      console.log(res);
      debugger;
      this.setState({ cleaning: res.data });
    })

  }

  render() {

    console.log("Detalhes " + JSON.stringify(this.state.cleaning[0]))
    debugger;
  return (
    <div>
      <div class="container">
      {this.state.cleaning.map(clean => (
        <div class="row card">
              <div class=" col-lg-12">
                  <p class="titulo">Detalhes da Piscina de {clean[0].name_client} </p>
              </div>
              <div class=" card col-lg-12">
                  <div class="col-md-6">
                    <p><i>Antes</i></p>
                    <img class="card-img-top img-fluid img-responsive" src={this.url + "/assets/uploads/" + clean[0].file} alt=""/>
                  </div>
                  <div class="col-md-6">
                    <p><i>Depois</i></p>
                    <img class="card-img-top img-fluid img-responsive" src={this.url + "/assets/uploads/" + clean[1].file} alt=""/>
                  </div>
                  <div class="col-md-12">
                    <div class="card-body">
                      <h3 class="card-title">Piscina de <b>{clean[0].name_client}</b></h3>
                      <h4>Periodo de Limpeza <span>De <b>{clean[0].check_in}</b> até <b>{clean[0].check_out}</b> </span></h4>
                      
                    </div>
                  </div>
              </div>
              <div class=" col-lg-12">
                  <p class="titulo">Detalhes da limpeza</p>
              </div>
              <div class="card col-lg-12">
                <div class="card-outline-secondary my-4">
                  <div class="card-header">
                  <p class="card-text"><h4><b><i>O que foi feito?</i></b> </h4></p>
                  </div>
                  <div class="card-body">
                    <div class="col-md-6">
                      <p>{clean[0].description}</p>
                    </div>
                    <div class="col-md-6">
                      <p><b>Nível de Cloro Livre:</b> {clean[0].tot_chlorine}</p>
                      <p><b>Nível de PH:</b> {clean[0].level_ph}</p>
                      <p><b>Nível de Alcalinidade:</b> {clean[0].level_alkalinity}</p>
                      <p><b>Nível de Dureza Cálcacia:</b> {clean[0].level_hardness}</p>
                    </div>
                    
                    <hr/>
                  </div>
                </div>
              </div>
              <div class=" col-lg-12">
                  <p class="titulo">Impedimentos <b>(Importante)</b> </p>
              </div>
              <div class="card col-lg-12">
                <div class="col-md-12">
                    <p> <b>Os itens listados aqui de alguma forma impedem o tratamento correto da piscina.</b> </p>
                </div>
                <div class="card col-lg-4 col-xs-12">
                  <p class="sub-titulo"><i>Registro Rachado</i></p>
                  <img class="card-img-top img-fluid img-responsive" src="https://br.habcdn.com/photos/business/medium/piscina02-1974244.jpg" alt=""/>
                </div>
                <div class=" card col-lg-4 col-xs-12">
                  <p class="sub-titulo"><i>Motor Estragado</i></p>
                  <img class="card-img-top img-fluid img-responsive" src="https://br.habcdn.com/photos/business/medium/piscina02-1974244.jpg" alt=""/>
                </div>
                <div class=" card col-lg-4 col-xs-12">
                  <p class="sub-titulo"><i>Motor Estragado</i></p>
                  <img class="card-img-top img-fluid img-responsive" src="https://br.habcdn.com/photos/business/medium/piscina02-1974244.jpg" alt=""/>
                </div>
              </div>
        </div>     
      ))}
      </div>    
    </div>
  )};
}

export default DetailCleaning;
