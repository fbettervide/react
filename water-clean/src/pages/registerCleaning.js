import React, { Component } from 'react';
import { connect } from 'react-redux';

import { makeStyles } from '@material-ui/core/styles';
import Layout from '../Layout';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import axios from 'axios';
import styles from '../css/register-pool.css';
import ScheduleList from '.';
import { url } from '../path';
   
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  button: {
    margin: theme.spacing(1),
  },
  leftIcon: {
    marginRight: theme.spacing(1),
  },
  rightIcon: {
    marginLeft: theme.spacing(1),
  },
  iconSmall: {
    fontSize: 20,
  },
}));

class RegisterCleaning extends Component {

  url  = url(); 

  constructor(props) {
    super(props);

    console.log('PROPS ' + JSON.stringify(props))

    this.state = {
      register: 0,
      idPool: this.props.idPool,
      cleaning : [] 
    };

    this.onChange = this.onChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  
  classes() {
    return useStyles();
  } 

  onChange(event) {
    debugger;
    switch (event.target.name) {
      case 'description':
        this.state.cleaning.description = event.target.value;
        break;
      case 'imagePoolIn':
          let files = event.target.files;

          let reader = new FileReader();
          reader.readAsDataURL(files[0]);
  
          reader.onload=(e) => {
            debugger;
            this.state.cleaning.imagePoolIn = e.target.result;  
          }
        break;
      case 'imagePoolOut':
          let files2 = event.target.files;

          let reader2 = new FileReader();
          reader2.readAsDataURL(files2[0]);
  
          reader2.onload=(e) => {
            debugger;
            this.state.cleaning.imagePoolOut = e.target.result;  
          }
        break;  
      case 'checkIn':
        this.state.cleaning.checkIn = event.target.value;
        break;  
      case 'imagePoolOut':
        this.state.cleaning.imagePoolOut = event.target.value;
        break;  
      case 'checkOut':
        this.state.cleaning.checkOut = event.target.value;
        break;
      case 'select-ph':
        this.state.cleaning.levelPh = event.target.value;
        break;   
      case 'select-chlorine':
        this.state.cleaning.levelChlorine = event.target.value;
        break; 
      case 'select-alca':
        this.state.cleaning.levelAlkalinity = event.target.value;
        break;
      case 'select-dureza':
        this.state.cleaning.levelHardness = event.target.value;
        break;
    }
    
  }

  handleSubmit(event) {
    event.preventDefault();
    debugger;
    axios
        .post(this.url + "/limpeza/registrar", {
          "description": this.state.cleaning.description,
          "image": this.state.cleaning.imagePoolIn,
          "check_in": this.state.cleaning.checkIn,
          "image2": this.state.cleaning.imagePoolOut,
          "check_out": this.state.cleaning.checkOut,
          "level_chlorine": this.state.cleaning.levelChlorine,
          "level_ph":  this.state.cleaning.levelPh,
          "level_alkalinity" : this.state.cleaning.levelAlkalinity,
          "level_hardness": this.state.cleaning.levelHardness,
          "pool_id": this.state.idPool
        })
        .then(function(response) {
            debugger;
            if(response) {
              window.location.reload();
              this.state.register = true;
              
            }
            debugger;
            console.log(response);
        }) .catch(function (error) {
            console.log(error);
        });
  }

  componentDidMount() {
    axios.get(this.url + '/piscinas/limpezas/' + this.state.idPool).then(res => {
      console.log(res);
      this.setState({ cleans: res.data[0] });
    })
  }

  
  render() {

    var style = {
      textAlign: 'center'
    };

  return (
    <div>
      { !this.state.register ? 
      <div class="container">
        <div class="row card">
        <form class="form-horizontal" onSubmit={this.handleSubmit} >
            <legend>Registro de Limpeza</legend>
            <div class="form-group ">
              <div class="col-md-offset-2 col-md-4 col-xs-12">
                <label class="control-label" for="imagePool">Imagem da Piscina (Chegada)</label>
                <input id="imagePool" name="imagePoolIn" class="input-file form-control" onChange={this.onChange} type="file" />
              </div>
              <div class="col-md-4 col-xs-12">
                <label class="control-label" for="textinput">Chegada:</label>  
                <input id="textinput" name="checkIn" type="time" placeholder="placeholder" onChange={this.onChange} class="form-control input-md" />
              </div>
            </div>
            <hr/>
            <div class="form-group">
              <div class="col-md-offset-2 col-md-4 col-xs-12">
                <label class="control-label" for="imagePool">Imagem da Piscina (Saída)</label>
                <input id="imagePool" name="imagePoolOut" class="input-file form-control" onChange={this.onChange} type="file" />
              </div>
              <div class="col-md-4 col-xs-12">
                <label class="control-label" for="textinput">Saída:</label>  
                <input id="textinput" name="checkOut" type="time" placeholder="placeholder" onChange={this.onChange} class="form-control input-md" />
              </div>
            </div>
            <hr/>
            <div class="form-group">
              <div class="col-md-offset-2 col-md-8 col-xs-12">                     
                <label class="control-label" >Descrição</label>
                <textarea class="form-control" id="description" name="description" onChange={this.onChange} >Descrição da limpeza</textarea>
              </div>
            </div>
            <hr/>
            <div class="form-group">
              <div class="col-md-offset-2 col-md-4 col-xs-12">                     
                <label class="control-label" >Cloro Livre</label>
                <select class="form-control" id="select-chlorine" name="select-chlorine" onChange={this.onChange} >
                  <option value="0.5">0,5</option>
                  <option value="1">1</option>
                  <option value="2">2</option> 
                  <option value="3">3</option>
                  <option value="4">4</option> 
                  <option value="5">5</option>
                  <option value="10">10</option> 
                  <option value=">10">> 10</option> 
                </select>
              </div>
              <div class="col-md-4 col-xs-12">                     
                <label class="control-label" >Nível PH</label>
                <select class="form-control" id="select-ph" name="select-ph" onChange={this.onChange} >
                  <option value="6">6</option>
                  <option value="6.8">6,8</option>
                  <option value="7">7</option> 
                  <option value="7.2">7,2</option>
                  <option value="7.4">7,4</option> 
                  <option value="7.6">7,6</option>
                  <option value="7.8">7,8</option> 
                  <option value="8">8</option> 
                  <option value="8.2">8,2</option> 
                  <option value=">8.2">> 8,2</option> 
                </select>
              </div>
              <div class="col-md-offset-2 col-md-4 col-xs-12">                     
                <label class="control-label" >Alcalinidade</label>
                <select class="form-control" id="select-alca" name="select-alca" onChange={this.onChange} >
                <option value="10">10</option>
                  <option value="20">20</option>
                  <option value="30">30</option> 
                  <option value="40">40</option>
                  <option value="50">50</option> 
                  <option value="60">60</option>
                  <option value="70">70</option> 
                  <option value="80">80</option> 
                  <option value="90">90</option> 
                  <option value="100">100</option> 
                  <option value="110">110</option> 
                  <option value="120">120</option> 
                  <option value="150">150</option> 
                  <option value="180">180</option> 
                  <option value="240">240</option> 
                  <option value=">240">> 240</option> 
                </select>
              </div>
              <div class="col-md-4 col-xs-12">                     
                <label class="control-label" for="textarea">Dureza Cálcica</label>
                <select class="form-control" id="select-dureza" name="select-dureza" onChange={this.onChange} >
                  <option value="0">0</option>
                  <option value="10">10</option>
                  <option value="20">20</option> 
                  <option value="50">50</option> 
                  <option value="100">100</option>
                  <option value="150">150</option> 
                  <option value="200">200</option> 
                  <option value="250">250</option> 
                  <option value="300">300</option> 
                  <option value="350">350</option> 
                  <option value="400">400</option> 
                  <option value="450">450</option> 
                  <option value="500">500</option> 
                  <option value="550">550</option> 
                  <option value="600">600</option> 
                  <option value="650">650</option> 
                  <option value="700">700</option> 
                  <option value="750">750</option> 
                  <option value="800">800</option> 
                  <option value=">800">> 800</option> 
                </select>
              </div>
            </div> 
            <div class="form-group">
              <div class="col-md-12" style={style}>
                <button id="singlebutton" name="singlebutton" type="submit" class="btn btn-primary">Registrar Limpeza</button>
              </div>
            </div>
            <hr/>
          </form>
        </div>
      </div>    
      : <ScheduleList /> }
    </div>
    
  )};
}

const mapStateToProps = (state) => ({ data: state });

export default connect(mapStateToProps)(RegisterCleaning);
