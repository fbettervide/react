import React, { Component } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Layout from '../Layout';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import axios from 'axios';
import { url } from '../path';
   
function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  button: {
    margin: theme.spacing(1),
  },
  leftIcon: {
    marginRight: theme.spacing(1),
  },
  rightIcon: {
    marginLeft: theme.spacing(1),
  },
  iconSmall: {
    fontSize: 20,
  },
}));

class Cleaning extends Component {

  url  = url();

  constructor(props) {
    super(props);

    this.state = {
      value: 0,
      setValue: 0,
      idPool: this.props.match.params.id,
      cleans: [],
      date: this.getDate(),
      cleaning : [] 
    };

    this.handleChange = this.handleChange.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onClick = this.onClick.bind(this);
  }
  
  
  classes() {
    return useStyles();
  } 

  handleChange(event, newValue) {
    this.setState({ 
      setValue: newValue,
      value: newValue,
    });
  }

  onChange(event) {
    switch (event.target.name) {
      case 'description':
        this.state.cleaning.description = event.target.value;
        break;
      case 'checkIn':
        this.state.cleaning.checkIn = event.target.value;
        break;
      case 'checkOut':
        this.state.cleaning.checkOut = event.target.value;
        break;
      case 'levelChlorine':
        this.state.cleaning.levelChlorine = event.target.value;
        break; 
      case 'levelAlkalinity':
        this.state.cleaning.levelAlkalinity = event.target.value;
        break;
      case 'levelConditioner':
        this.state.cleaning.levelConditioner = event.target.value;
        break;
      case 'levelHardness':
        this.state.cleaning.levelHardness = event.target.value;
        break;
      case 'levelPhosphate':
        this.state.cleaning.levelPhosphate = event.target.value;
        break;
    }
    
  }

  onClick() {
    console.log(this.state);
    axios
        .post(this.url + "/limpeza/registrar", {
          "description": this.state.cleaning.description,
          "check_in": this.state.cleaning.checkIn,
          "check_out": this.state.cleaning.checkOut,
          "tot_chlorine": this.state.cleaning.levelChlorine,
          "level_ph": '7',
          "level_alkalinity" : this.state.cleaning.levelAlkalinity,
          "level_conditioner": this.state.cleaning.levelConditioner,
          "level_hardness": this.state.cleaning.levelHardness,
          "level_phosphate": this.state.cleaning.levelPhosphate,
          "pool_id": this.state.idPool
        })
        .then(function(response) {
            console.log(response);
        }) .catch(function (error) {
            console.log(error);
        });
  }

  componentDidMount() {
    debugger;
    axios.get(this.url + '/piscinas/limpezas/' + this.state.idPool).then(res => {
      console.log(res);
      this.setState({ cleans: res.data[0] });
    })
    debugger;
  }

  getDate() {
    return Date();
  }

  render() {

    console.log("LIMPEZA " + JSON.stringify(this.state.cleans));

  return (
    <div>
      <div class="container">
        <div class="row">
          { this.state.cleans[0] ?
          this.state.cleans.map(clean => (
            <div class="col-sm-6 col-md-3">
              <div class="thumbnail">
                <img src="https://img.olx.com.br/images/72/728806117572506.jpg" alt="..." />
                <div class="caption">
                  <h3> {clean.description}</h3>
                  <p><span>Inicio:</span> {clean.check_in} | <span>Fim:</span> {clean.check_out}</p>
                  <p><span>Data de Realização:</span> {clean.created_at} {clean.id}</p>
                  <p><a href={"/detalhes/" + clean.id } class="btn btn-primary " role="button">Ver Detalhes</a></p>
                </div>
              </div>
            </div>
          ))  
          : 
          <div class="alert alert-info">
            <strong>Ops!</strong> Nenhuma limpeza foi realizada para esse cliente.
          </div>}
        </div>
      </div>    
    </div>
  )};
}

export default Cleaning;
