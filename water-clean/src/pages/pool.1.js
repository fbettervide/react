import React, { Component } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Layout from '../Layout';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import red from '@material-ui/core/colors/red';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import axios from 'axios';
import styles from '../css/pool.css';
import { Link } from '@material-ui/core';
   
class PoolList extends Component {

  state = {
    pools: []
  };

  componentDidMount() {
    axios.get('http://127.0.0.1/piscinas').then(res => {
      console.log(res);
      this.setState({ pools: res.data });
    })
  }

  initialName(stringExemplo) {
    return stringExemplo.substring(1, 0);
  }

  render() {
    return (
      <div>
        <div>
          <Layout />
        </div>
        {this.state.pools.map(pool => (
          <div className="margin">
          <Card className={styles.card} >
            <CardHeader
              avatar={
                <Avatar aria-label="Recipe" className={styles.avatar}>
                  {this.initialName(pool.name_client)}
                </Avatar>
              }
              action={
                <IconButton aria-label="Settings">
                  <MoreVertIcon />
                </IconButton>
              }
              title={pool.address.logradouro}
              subheader={pool.address.neighborhood}
            />
            <CardMedia
              className="image"
              image="https://img.olx.com.br/images/72/728806117572506.jpg"
              title="Paella dish"
            />
            <CardContent>
              <Typography variant="body2" color="textSecondary" component="p">
                {pool.description}
              </Typography>
            </CardContent>
            <CardActions disableSpacing>
              <IconButton aria-label="Add to favorites">
                <FavoriteIcon />
              </IconButton>
              <IconButton aria-label="Share">
                <ShareIcon />
              </IconButton>
              <Link href=""></Link>
            </CardActions>
            <Collapse in={this.expanded} timeout="auto" unmountOnExit>
              <CardContent>
                <Typography paragraph>Descrição:</Typography>
                <Typography paragraph>
                  Heat 1/2 cup of the broth in a pot until simmering, add saffron and set aside for 10
                  minutes.
                </Typography>
                <Typography paragraph>
                  Heat oil in a (14- to 16-inch) paella pan or a large, deep skillet over medium-high
                  heat. Add chicken, shrimp and chorizo, and cook, stirring occasionally until lightly
                  browned, 6 to 8 minutes. Transfer shrimp to a large plate and set aside, leaving chicken
                  and chorizo in the pan. Add pimentón, bay leaves, garlic, tomatoes, onion, salt and
                  pepper, and cook, stirring often until thickened and fragrant, about 10 minutes. Add
                  saffron broth and remaining 4 1/2 cups chicken broth; bring to a boil.
                </Typography>
                
              </CardContent>
            </Collapse>
          </Card>
          </div>
        ))}
      </div>
    );

  }

}


export default PoolList;