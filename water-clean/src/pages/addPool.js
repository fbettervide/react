import React, { Component } from 'react';
import { connect } from 'react-redux';

import { makeStyles } from '@material-ui/core/styles';
import Layout from '../Layout';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import axios from 'axios';
import { url } from '../path';
import { getData, setModalState ,addPool  } from '../actions/poolActions';


class AddPool extends Component {

  constructor(props) {
    super(props);

    this.state = {
      value: 0,
      setValue: 0,
      register : [],
      states : [], 
      update: false
    };

    this.onChange = this.onChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  url  = url();

  onChange(event) {

    console.log("NAMES INPUTS > " + event.target.name);

    switch (event.target.name) {
      case 'imagePool':
        let files = event.target.files;

        let reader = new FileReader();
        reader.readAsDataURL(files[0]);

        reader.onload=(e) => {
          this.state.register.image = e.target.result;  
        }

        break;
      case 'nameClient':
        this.state.register.nameClient = event.target.value;
        break;
      case 'description':
        this.state.register.description = event.target.value;
        break;
      case 'email':
        this.state.register.email = event.target.value;
        break;
      case 'phone':
        this.state.register.phone = event.target.value;
        break; 
      case 'zip':
        this.state.register.zip = event.target.value;
        break;
      case 'complement':
        this.state.register.complement = event.target.value;
        break;
      case 'logradouro':
        this.state.register.logradouro = event.target.value;
        break;
      case 'number':
        this.state.register.number = event.target.value;
        break;
      case 'neighborhood':
        this.state.register.neighborhood = event.target.value;
        break;  
      case 'state':
          this.state.register.state = event.target.value;
          axios.get(this.url + '/cities/' + event.target.value).then(res => {
            this.setState({ cities: res.data });
          })
          break; 
      case 'city':
          this.state.register.city = event.target.value;
          break;      
    }
    
  }

  handleSubmit(event) {
    event.preventDefault();

    let states  = this;

    const config = {
      headers: {
        'Content-Type': "application/json"
      }
    }

    axios
        .post(this.url + "/piscinas/add", {
          "name_client": this.state.register.nameClient,
          "description": this.state.register.description,
          "image": this.state.register.image,
          "status": "1",
          "swimming_pool_id": 1,
          "address": {
              "logradouro": this.state.register.logradouro,
              "number": this.state.register.number,
              "neighborhood": this.state.register.neighborhood,
              "complement": this.state.register.complement,
              "latitude": "",
              "longitude": "",
              "city_id": this.state.register.city
          },
          "contacts": [
            {
                "type": "3",
                "value": this.state.register.email,
                "status": "1"
            },{
                "type": "1",
                "value": this.state.register.phone,
                "status": "1"
            }
          ]
        }, config)
        .then(function(response) {

            if(response) {
              states.setState({ update: true })
            }
        }) .catch(function (error) {
            console.log(error);
        });

  }

  componentDidMount() {
    axios.get(this.url + '/states').then(res => {
      this.setState({ states: res.data });
    })
  }

  

  render() {
    
    if(this.state.update) {
      this.props.dispatch(setModalState(false));
      this.props.updatePools();
    }
    
    return (
      <div> 
          <div class="card row col-md-12">
          <form class="form-horizontal" onSubmit={this.handleSubmit} >
  
              <legend>Cadastro de Piscinas</legend>
              <div class="col-md-12">
                <div class="form-group">
                  <div class="col-md-12">
                    <label class="control-label" >Imagem da Piscina </label>
                    <input id="imagePool" name="imagePool" onChange={this.onChange} class="input-file" type="file" />
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-md-12">
                    <label class="control-label" for="nameClient">Nome do Cliente</label>  
                    <input id="nameClient" onChange={this.onChange} name="nameClient" type="text" placeholder="Proprietário" class="form-control input-md" />
                  </div>
                </div>
    
                <div class="form-group">
                  <div class="col-md-12">
                    <label class="control-label" for="description">Descrição</label>  
                    <textarea class="form-control" id="description" onChange={this.onChange} name="description" placeholder="Descrição da Piscina"></textarea>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-md-6">
                    <label class="control-label" for="email">Email</label>
                    <input id="email" onChange={this.onChange} name="email" type="text" placeholder="Email" class="form-control input-md" />
                  </div>
                  <div class="col-md-6">
                    <label class="control-label" for="phone">Celular</label>  
                    <input id="phone" onChange={this.onChange} name="phone" type="text" placeholder="Ex.: (51) 9999-9999" class="form-control input-md" />
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-md-6">
                    <label class="control-label" for="zip">Cep</label>  
                    <input id="zip" onChange={this.onChange} name="zip" type="text" placeholder="Cep" class="form-control input-md" />
                  </div>
                  <div class="col-md-6">
                    <label class="control-label" for="complement">Complemento</label>  
                    <input id="complement" onChange={this.onChange} name="complement" type="text" placeholder="Complemento" class="form-control input-md" />
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-6">
                    <label class="control-label" for="logradouro">Rua</label>  
                    <input id="logradouro" onChange={this.onChange} name="logradouro" type="text" placeholder="Rua" class="form-control input-md" />
                  </div>
                  <div class="col-md-3">
                    <label class="control-label" for="number">Numero</label>    
                    <input id="number" onChange={this.onChange} name="number" type="text" placeholder="Numero Res." class="form-control input-md" />
                  </div>
                  <div class="col-md-3">
                    <label class="control-label" for="neighborhood">Bairro</label>  
                    <input id="neighborhood" onChange={this.onChange} name="neighborhood" type="text" placeholder="Bairro" class="form-control input-md" />
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-md-6">
                    <label class="control-label" for="city">Cidade</label>  
                    <select id="city" onChange={this.onChange} name="city" class="form-control input-md" >
                    {this.state.cities ? 
                      this.state.cities.map((city) => (
                        <option value={city.id}> {city.name} </option>
                      ))
                    : <option value=""> Selecione ... </option>}
                    </select>
                  </div>
                  <div class="col-md-6">
                    <label class="control-label" for="state">Estado</label>  
                    <select name="state" onChange={this.onChange} class="form-control input-md" >
                    {this.state.states.map((sta) => (
                      <option value={sta.id}> {sta.name} </option>
                    ))}
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-12">
                    <button id="singlebutton" type="submit" name="singlebutton"  class="btn btn-primary pull-right">Cadastrar Piscina</button>
                  </div>
                </div>
              </div>
            </form>
          </div> 
      </div>
    )};
}

const mapStateToProps = (state) => ({ data: state });

export default connect(mapStateToProps)(AddPool);
