import React, { Component } from 'react';
import { connect } from 'react-redux';
import Layout from '../Layout';
import axios from 'axios';
import styles from '../css/pool.css';
import AddPool from './addPool';
import Modal from 'react-modal';
import ModalWrap from '../Modal';
import { getData, setModalState, addPool  } from '../actions/poolActions';
import { url } from '../path';
import Schedule from '../css/schedule.css';
import ScheduleAdd from './scheduleAdd';
import ListPool from './listPool';
   
Modal.setAppElement('#root');
class Pool extends Component {

  url  = url();

  constructor(props) {
    super(props);

    this.state = {
      value: 0,
      setValue: 0,
      register : [],
      states : [], 
      returnRegister: false,
      open: false,
      update: false,
      pools: [],
      add: false,
      agenda: false,
      poolSelected: 0
    };
    
    this.upgradePool = this.upgradePool.bind(this);
    this.openModal = this.openModal.bind(this);
    
  }

  selecModal(data) {
    if(data.currentTarget.value == "add") {
      this.state.agenda = false;
      this.state.add = true;
    } else {
      this.state.poolSelected = data.currentTarget.value; 
      this.state.add = false;
      this.state.agenda = true;
    }
  }
  
  openModal = (data=null) => {
    this.selecModal(data);
    this.props.dispatch(setModalState(true));
  }

  componentDidMount() {
    
    this.upgradePool();
    axios.get(this.url + '/states').then(res => {
      this.setState({ states: res.data });
      
    })
  }

  upgradePool() {
    axios.get(this.url + '/piscinas').then(res => {
      this.setState({ 
        ...this.state,
        pools: res.data 
      });
      
    })
    
  }


  render() {
    
    var button = {
      marginTop: '10px'
    };

    return (
      <div>
        <ModalWrap modalTitle={ 'Cadastro de Piscina' }>
          { this.state.add ? 
            <AddPool updatePools={this.upgradePool} pools={this.props.children} />
          : this.state.agenda ? 
            <ScheduleAdd props={this.props} idPool={this.state.poolSelected} />
          : ""}
        </ModalWrap>
        <div className="container">
          <div className="col-md-12 col-xs-12 " >
              <button id="btn-add" className="btn btn-primary pull-right" onClick={this.openModal} value="add"> Adicionar Piscina</button>
          </div>
        </div>
        {this.state.pools[0] ? 
        <ListPool openModal={this.openModal} >
          {this.state.pools}
        </ListPool>  
        : 
          <div className="container">
            <br/>
            <div className="alert alert-info">
              Nenhuma piscina cadastrada no momento.
            </div>
          </div>
        }
      </div>
    );

  }

}

const mapStateToProps = (state) => ({ data: state });

export default connect(mapStateToProps)(Pool);
//export default Pool;