import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { getData, setModalState, addPool  } from '../actions/poolActions';

import axios from 'axios';
import { url } from '../path';
   
class ScheduleAdd extends Component {

  url  = url(); 

  constructor(props) {
    super(props);

    console.log('PROPS SCHEDULE ' + JSON.stringify(props))

    this.state = {
      register: 0,
      idPool: this.props.idPool,
      schedule : [],
      redirect: false 
    };

    this.onChange = this.onChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  

  onChange(event) {
    switch (event.target.name) {
      case 'dataVisit':
        this.state.schedule.dataVisit = event.target.value;
        break;
      case 'horaVisit':
        this.state.schedule.horaVisit = event.target.value;
        break;
    }
    
  }

  handleSubmit(event) {
    event.preventDefault();

    let states  = this;

    axios
        .post(this.url + "/agenda/add", {
          "date_check": this.state.schedule.dataVisit,
          "time_check": this.state.schedule.horaVisit,
          "status": 0,
          "pool_id": this.state.idPool
        })
        .then(function(response) {
            if(response) {
              debugger;
              states.setState({ redirect: true });
              //this.props.history.push('/agenda')
              
            }
        }) .catch(function (error) {
            console.log(error);
        });
  }

  render() {

    console.log("PRO " + this.props);

    if(this.state.redirect) {
      this.props.props.dispatch(setModalState(false));
      this.props.props.history.push('/agenda');
    }

    var style = {
      textAlign: 'center'
    };

  return (
    <div>
      <div class="container">
        <div class="row">
        <form class="form-horizontal" onSubmit={this.handleSubmit} >

            <legend>Agendar Limpeza</legend>
            <div class="form-group ">
              <div className="col-md-offset-3 col-md-6 col-xs-12">
                <div className="col-md-12 col-xs-12">
                  <label className=" control-label" for="dataVisit">Data da Visita</label>  
                  <div className="col-md-12 col-xs-12">
                    <input id="dataVisit" name="dataVisit" type="date" placeholder="Data da Visita" onChange={this.onChange} className="form-control input-md"  />
                  </div>
                </div>
                <hr/>
                <div className="col-md-12 col-xs-12">
                  <label className=" control-label" for="horaVisit">Horário da Visita</label>
                  <div className="col-md-12 col-xs-12">
                    <input id="horaVisit" name="horaVisit" type="time"  placeholder="Hora da Visita" onChange={this.onChange} className="input-file form-control"  />
                  </div>
                </div>
              </div>
              <hr/>
            </div>
            <div class="form-group ">
              <div className="form-group">
                <div className="col-md-12 col-xs-12" style={style}>
                  <button id="singlebutton" name="singlebutton" type="submit" className="btn btn-primary">Agendar Limpeza</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>    
    </div>
    
  )};
}

const mapStateToProps = (state) => ({ data: state });

export default connect(mapStateToProps)(ScheduleAdd);
